package com.techu.practica.practica.repository;

import com.techu.practica.practica.model.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
@Repository public interface ProductoRepository extends MongoRepository<ProductoModel, String> { }